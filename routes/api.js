const express = require("express");
const router = express.Router();
const path = require("path");
const iplData = require("../fetchData.js")

router.get("/seasons", function(req, res){
	iplData.getSeasons().then(function(data){
		res.send(data);
	})
});


router.get("/seasons/:season", function(req, res){
	iplData.getTeams(req.params.season).then(function(data){
		res.send(data);
	})
});

router.get("/seasons/:season/:team", function(req, res){
	let season = req.params.season;
	let team = iplData.getFullName(req.params.team);
	iplData.getPlayers(season, team).then(function(data){
		res.send(data);
	})
});

router.get("/seasons/:season/:team/:player", function(req, res){
	let player = req.params.player;
	player = player.replace(/-/g, ' ');
	// console.log(player);
	iplData.playerDetails(player).then(function(data){
		res.send(data);
	})
});

router.get("/seasons", function(req, res){

})


module.exports = router;
