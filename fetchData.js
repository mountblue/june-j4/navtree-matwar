const MongoClient = require("mongodb").MongoClient;
const url = "mongodb://127.0.0.1:27017";

// function getSeasons(){
//     return new Promise(function(resolve, reject){
//         MongoClient.connect(url, {useNewUrlParser: true}, function(err, db){
//             if(err){
//                 console.log(err.message);
//             }

//             let dbo = db.db("ipl");
//             let collection = dbo.collection("matches");

//             collection.distinct("season", function(err, res){
//                 if(err){
//                     console.log(err.message);
//                 }
//                 res.sort(function(a,b){return a-b;});//console.log(res);
//                 resolve({data:res});
//             })
//         })
//     })
// }

function getFullName(name){
    let obj = {
        mi: "Mumbai Indians",
        gl: "Gujarat Lions",
        srh: "Sunrisers Hyderabad",
        rps: "Rising Pune Supergiant",
        kkr: "Kolkata Knight Riders",
        rcb: "Royal Challengers Bangalore",
        dd: "Delhi Daredevils",
        kip: "Kings XI Punjab",
        rr: "Rajasthan Royals",
        dc: "Deccan Chargers",
        csk: "Chennai Super Kings",
        ktk: "Kochi Tuskers Kerala",
        pw: "Pune Warriors"
    }

    return obj[name];
}

function getSeasons(){
    return new Promise(function(resolve, reject){
        MongoClient.connect(url, {useNewUrlParser: true}, function(err, db){
            if(err){
                console.log(err.message);
            }

            let dbo = db.db("ipl");
            let collection = dbo.collection("matches");

            collection.aggregate([{$group:{_id: "$season"}},{$project:{year:"$_id", _id:0}},{$sort:{year:1}}]).toArray(function(err, res){
                if(err){
                    console.log(err.message);
                }
                resolve({data:res});
            })
        })
    })
}


function getTeams(season){
    return new Promise(function(resolve, reject){
        MongoClient.connect(url, {useNewUrlParser: true}, function(err, db){
            if(err){
                console.log(err)
            }
            let dbo = db.db("ipl");
            let collection = dbo.collection("matches")
            let seas = season;
            collection.aggregate([{$match:{"season": parseInt(season)}}, {$group:{_id: "$team1"}},{$project:{team:"$_id", _id:0}}]).toArray(function(err, docs){
                if(err){
                    console.log(err.message)
                }
                resolve({data:docs});
            })
        })
    })
}

function getPlayers(season, team){
    return new Promise(function(resolve, reject){
            MongoClient.connect(url,{useNewUrlParser: true}, function(err, db){
            if(err){
                console.log(err.message);
            }
            let dbo = db.db("ipl");
            let collection = dbo.collection("matches");
            //console.log(team)
            let match1 = {"$match":{"season": parseInt(season)}};
            let lookup = {
                    $lookup: {
                        from: "deliveries",
                        localField: "id",
                        foreignField: "match_id",
                        as: "deliveriesDetails"
                    }
                }
            let unwind = {"$unwind":"$deliveriesDetails"};
            let project1 = {
                $project: {
                    batsman: "$deliveriesDetails.batsman",
                    batting_team: "$deliveriesDetails.batting_team"
                }
            }
            let match2 = {"$match":{"batting_team": team}};
            let group = {
                        "$group":{"_id": "$batsman"
                        }
                    };
            let project2 = {
                        $project:{
                            name: "$_id",
                            _id: 0
                            }
                        };
            collection.aggregate([
                match1, lookup, unwind, project1, match2, group, project2
            ]).toArray(function(err, data){
                if(err){
                    console.log(err)
                }//console.log(data)
                resolve(data);
            })

        })
    })
}


function playerDetails(player){
    return new Promise(function(resolve, reject){
        MongoClient.connect(url,{useNewUrlParser: true}, function(err, db){
            if(err){
                console.log(err.message);
            }
            let dbo = db.db("ipl");
            let collection = dbo.collection("deliveries");
            //console.log(team)
            let match = {"$match":{"batsman": player}};

           // let match2 = {"$match":{"batting_team": team}};
            let group = {
                        "$group":{"_id": "$match_id"
                        },
                        // matches: {$sum: 1},
                        // runs: { $sum: "$batsman_runs"}
                    };
            let project2 = {
                        $project:{
                            player: "$_id",
                            _id: 0,
                            runs: 1,
                            }
                        };
            collection.aggregate([
                match, group
            ]).toArray(function(err, data){
                if(err){
                    console.log(err)
                }//console.log(data)
                resolve({data: data.length});
            })

        })
    })
}
// playerDetails("TM Dilshan");
// getPlayers(2008, "Royal Challengers Bangalore");
// getSeasons();
// getTeams(2016).then(function(d){
//     console.log(d);
// });

module.exports = {
    getSeasons: getSeasons,
    getTeams: getTeams,
    getFullName: getFullName,
    getPlayers: getPlayers,
    playerDetails: playerDetails
}
