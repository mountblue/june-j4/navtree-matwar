function getShortName(name){
	if(name == "Kings XI Punjab"){
		return "kip";
	}
	if(name == "Sunrisers Hyderabad"){
		return "srh";
	}
	return name.split(' ').map(function(item){return item[0]}).join('').toLowerCase();
}

function removeSpace(name){
	return name.split(" ").join("-");
}
function removeDash(name){
	return name.replace(/-/g, ' ');
}
function getSeasons(){
	return $.ajax({
	  type: 'GET',
	  url: '/api/seasons'

	});
}

// let getSeasons =  $.ajax({
// 		type: 'GET',
// 		url: '/api/seasons',
// 	});

function getTeams(year){
	return $.ajax({
	  type: 'GET',
	  url: `/api/seasons/${year}`
	});
}

function getPlayers(year, team){
	return $.ajax({
	  type: 'GET',
	  url: `/api/seasons/${year}/${team}`
	});
}

function getPlayerDetails(player){
	return $.ajax({
	  type: 'GET',
	  url: `/api/seasons/2008/rcb/${player}`
	});
}

let seasonsULList = $(".seasons");
let seasonsArray = [];
getSeasons().done(function(d){
	d.data.forEach(function(el, index){
		seasonsArray.push(el.year);
	})
	$.each(seasonsArray, function(i){
		$(".seasons").append(`<li class="season ${seasonsArray[i]}">IPL ${seasonsArray[i]}</li>`);
	})

});

$(".seasons").on('click', "li.season", function(event){
	event.stopPropagation();
	console.log($(".content").html());
	let self = this;
	let season = $(this).attr("class").substr(-4);
	if(!$(this).hasClass("open")){
		let teamsArray = [];
		getTeams(season).done(function(d){
			d.data.forEach(function(el, index){
				teamsArray.push(el.team);
			});//console.log(teamsArray);
			$(`<ul class="teams ${season}"></ul>`).insertAfter( $(self) );
			$.each(teamsArray, function(i){
				$(`.teams.${season}`).append(`<li class="team ${removeSpace(teamsArray[i])}">${teamsArray[i]}</li>`);
			})

		});
		$($(this)).addClass("open");
	}else if($(this).hasClass("open")){
		$(this).next().remove()
		$($(this)).removeClass("open");
	}

});

$(".seasons").on('click', "li.team", function(event){
	event.stopPropagation();
	let team = getShortName(removeDash($(this).attr("class").substr(5)));
	let season = $(this).parent().attr("class").substr(-4);
	let self = this;
	// console.log(team, season);

	if(!$(this).hasClass("open")){
		let playersArray = [];
		getPlayers(season, team).done(function(d){//console.log(d);
			d.forEach(function(el){
				playersArray.push(el.name);
			});//console.log(playersArray);
			$(`<ul class="players"></ul>`).insertAfter($(self));
			$.each(playersArray, function(i){
				$(".players").append(`<li class="player ${removeSpace(playersArray[i])}">${playersArray[i]}</li>`);
			})
		})
		$($(this)).addClass("open");
	}else if($(this).hasClass("open")){
		$(this).next().remove()
		$($(this)).removeClass("open");
	}

})


$(".seasons").on('click', "li.player", function(event){
	event.stopPropagation();

	let player = $(this).attr("class").substr(7);
	getPlayerDetails(player).done(function(d){
		$(".content").html(`MATCHES PLAYED: ${d.data}`);
	})
})
