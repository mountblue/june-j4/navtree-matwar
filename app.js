const express     = 	require("express");
const app         = 	express();
const path        = 	require("path");
// const routes      =


app.use(express.static("public"));
app.use("/api", require("./routes/api"));

app.get("/", function(req, res){
	res.redirect("/seasons")
});

app.get("/seasons", function(req, res){
	res.sendFile(path.resolve("views/home.html"));
});


app.listen(3000, function(){
	console.log("server started");
})
